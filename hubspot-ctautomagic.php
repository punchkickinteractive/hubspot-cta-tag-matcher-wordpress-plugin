<?php
/**
 * Plugin Name: Hubspot CTAutomagic
 * Plugin URI: https://bitbucket.org/danielwolf5/hubspot-cta-tag-matcher-wordpress-plugin
 * Description: This wordpress plugin will add calls to action from hubspot based on tags.
 * Version: 1.0
 * Author: Daniel Wolf
 * Author URI: https://bitbucket.org/danielwolf5
 * License: MIT
 */

global $hctm_db_version;
$hctm_db_version = '1.0';

/**
 * Install Script
 */
register_activation_hook(__FILE__, function ()
{
    global $wpdb;
    global $hctm_db_version;

    // DB table names
    $cta_table_name = $wpdb->prefix . 'hubspot_cta';
    $cta_tag_matches_table_name = $wpdb->prefix . 'hubspot_cta_tag_matches';

    // Determine charset and collation
    $charset_collate = '';
    if (!empty($wpdb->charset)) {
      $charset_collate = "DEFAULT CHARACTER SET {$wpdb->charset}";
    }
    if (!empty($wpdb->collate)) {
      $charset_collate .= " COLLATE {$wpdb->collate}";
    }

    // Create table sql
    $sql = "CREATE TABLE $cta_table_name (
            id mediumint(9) NOT NULL AUTO_INCREMENT,
            description varchar(255),
            call_to_action_js text NOT NULL,
            cta_order int(3) NOT NULL,
            PRIMARY KEY  id (id)
        ) $charset_collate;";
    $sql .= "CREATE TABLE $cta_tag_matches_table_name (
            {$cta_table_name}_id mediumint(9) NOT NULL,
            {$wpdb->prefix}term_id mediumint(9) NOT NULL,
            UNIQUE ({$cta_table_name}_id, {$wpdb->prefix}term_id)
        ) $charset_collate";

    // Using dbDelta to run sql
    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
    dbDelta($sql);

    // Storing db version for this plugin in case we need to make upgrades later
    add_option('hctm_db_version', $hctm_db_version);
});

/*
 * Admin Menu
 */
add_action('admin_menu', function ()
{
    add_menu_page(
        'Hubspot CTAutomagic', // page title
        'CTAutomagic', // menu title
        'manage_options', // perms required
        'hubspot-ctautomagic', // menu slug
        function ()
        {
            global $wpdb;

            // Perms check
            if (!current_user_can('manage_options'))  {
                wp_die(__('You do not have sufficient permissions to access this page.'));
            }

            // Form submitted?
            if (isset($_POST['submitted'])) {
                // Delete existings rows
                $wpdb->query("DELETE FROM {$wpdb->prefix}hubspot_cta;");
                $wpdb->query("DELETE FROM {$wpdb->prefix}hubspot_cta_tag_matches;");

                // Loop through rows submitted
                if ($_POST['desc']) {
                    foreach ($_POST['desc'] as $index => $desc) {
                        // If cta code is empty, we will not store this item
                        if (!$_POST['cta'][$index]) {
                            continue;
                        }
                        // Create row array and insert
                        $cta = array(
                            'description' => $desc,
                            'call_to_action_js' => $_POST['cta'][$index],
                            'cta_order' => $index
                        );
                        $wpdb->insert($wpdb->prefix . 'hubspot_cta', $cta);
                        // Get insert ID
                        $cta_id = $wpdb->insert_id;
                        // If tags were assigned to this item, loop through them
                        if (is_array($_POST['tags'][$index])) {
                            foreach ($_POST['tags'][$index] as $tag_id) {
                                // Create match array and insert
                                $match = array(
                                    $wpdb->prefix . 'hubspot_cta_id' => $cta_id,
                                    $wpdb->prefix . 'term_id' => $tag_id
                                );
                                $wpdb->insert($wpdb->prefix . 'hubspot_cta_tag_matches', $match);
                            }
                        }
                    }
                }
            }

            // Get data for display on form
            $tags = get_tags();
            $ctas = $wpdb->get_results(
                "SELECT id, call_to_action_js as js, description, GROUP_CONCAT({$wpdb->prefix}term_id) as tags
                FROM {$wpdb->prefix}hubspot_cta
                LEFT JOIN {$wpdb->prefix}hubspot_cta_tag_matches ON {$wpdb->prefix}hubspot_cta_tag_matches.{$wpdb->prefix}hubspot_cta_id = {$wpdb->prefix}hubspot_cta.id
                GROUP BY {$wpdb->prefix}hubspot_cta.id
                ORDER BY cta_order ASC"
            );
            // Explode comma sperated tags into an array for use on form
            foreach ($ctas as $index => $cta) {
                $ctas[$index]->tags = explode(',', $cta->tags);
            }
            require __DIR__ . '/admin-form.php';
        },
        plugin_dir_url(__FILE__) . 'css/hubspot-icon.png' // menu item icon url
    );
});


/**
 * Add scripts and style sheets
 */
add_action('admin_enqueue_scripts', function ()
{
    // Only load scripts and style sheets on the plugin page
    if ($_GET['page'] !== 'hubspot-ctautomagic') {
        return;
    }

    // js
    wp_enqueue_script(
        'hctm-chosen-script', plugin_dir_url(__FILE__) . 'js/chosen.jquery.min.js', array(), '1.0.0', true
    );
    wp_enqueue_script('hctm-script', plugin_dir_url(__FILE__) . 'js/script.js', array(), '1.0.0', true);

    // css
    wp_register_style('hctm-styles', plugin_dir_url(__FILE__) . 'css/style.css', false, '1.0.0');
    wp_enqueue_style('hctm-styles');
    wp_register_style('hctm-chosen-styles', plugin_dir_url(__FILE__) . 'css/chosen.min.css', false, '1.0.0');
    wp_enqueue_style('hctm-chosen-styles');
});

function hctm_render_cta()
{
    global $wpdb;

    // Get comma separated list of tag IDs
    $post_tags = wp_get_post_tags(get_the_ID());
    if (!$post_tags) {
        return;
    }
    $tags = implode(
        ',',
        array_map(function ($tag) {
            return $tag->term_id;
        }, $post_tags)
    );

    // Select first hubspot call to action match
    $row = $wpdb->get_row(
        "SELECT call_to_action_js
            FROM {$wpdb->prefix}hubspot_cta phc
            INNER JOIN {$wpdb->prefix}hubspot_cta_tag_matches phctm ON phc.id = phctm.{$wpdb->prefix}hubspot_cta_id
            WHERE phctm.pkiwp_term_id IN ({$tags})
            ORDER BY FIELD(phctm.pkiwp_term_id, {$tags})
            LIMIT 1"
    );
    if (isset($row->call_to_action_js)) {
        if ($row->call_to_action_js) {
            echo stripslashes($row->call_to_action_js);
        }
    }
}


