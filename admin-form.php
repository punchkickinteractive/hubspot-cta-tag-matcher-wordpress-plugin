<div id="hctm-container">
    <h1>Hubspot CTAutomagic</h1>
    <hr>
    <h3>Admin Panel</h3>
    <p>
        This plugin allows you to insert the embed code of a Hubspot call to
        action and have it render on every post that matches chosen tags.
    </p>
    <p>
        Enter a short description you can reference later, paste the embed code, and choose tags to associate.
        Matches will be made from top to bottom according to this list, so order is important.
    </p>
    <h3>CTA to Tag Matches</h3>
    <div class="wp-core-ui">
        <form id="hctm-form" action="" method="POST">
            <input type="hidden" name="submitted" value="1">
            <table class="form-table">
                <thead>
                    <tr>
                        <th>Description</th>
                        <th>Embed Code <span style="color:red">*</span></th>
                        <th>Tag(s)</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($ctas as $cta): ?>
                        <tr>
                            <td>
                                <input type="text" name="desc[]" value="<?php echo $cta->description; ?>" />
                            </td>
                            <td>
                                <textarea name="cta[]"><?php echo stripcslashes($cta->js) ?></textarea>
                            </td>
                            <td>
                                <select name="tags[]" multiple data-placeholder="Choose Some Tags...">
                                    <?php foreach($tags as $tag): ?>
                                        <option
                                            value="<?php echo $tag->term_id; ?>"
                                            <?php echo (in_array($tag->term_id, $cta->tags))? 'selected="selected"': ''; ?>
                                        >
                                            <?php echo $tag->name; ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                            </td>
                            <td>
                                <div>
                                    <button class="button hctm-up-row" title="Move Up">&#8593;</button>
                                    <button class="button hctm-down-row" title="Move Down">&#8595;</button>
                                    <button class="button hctm-delete-row" title="Remove">X</button>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><button class="button" id="new-cta-button">+ Add Row</button></td>
                    </tr>
                </tfoot>
            </table>
            <p class="submit"><button type="submit" class="button-primary button-large">Save Changes</button></p>
        </form>
    </div>
</div>

<script type="text/html" id="new-cta-template">
    <tr>
        <td>
            <input type="text" name="desc[]" value="" />
        </td>
        <td>
            <textarea name="cta[]"></textarea>
        </td>
        <td>
            <select name="tags[]" multiple data-placeholder="Choose Some Tags...">
                <?php foreach($tags as $tag): ?>
                    <option value="<?php echo $tag->term_id; ?>">
                        <?php echo $tag->name; ?>
                    </option>
                <?php endforeach; ?>
            </select>
        </td>
        <td>
            <div>
                <button class="button">&#8593;</button>
                <button class="button">&#8595;</button>
                <button class="button">X</button>
            </div>
        </td>
    </tr>
</script>