/**
 * Plugin Name: Hubspot CTAutomagic
 * Plugin URI: https://bitbucket.org/danielwolf5/hubspot-cta-tag-matcher-wordpress-plugin
 * Description: This wordpress plugin will add calls to action from hubspot based on tags.
 * Version: 1.0
 * Author: Daniel Wolf
 * Author URI: https://bitbucket.org/danielwolf5
 * License: MIT
 */


(function ($) {
    // storing template for new table row
    var newCtaTemplate = $('#new-cta-template').html();
    // 'add row' button functionality
    $('#new-cta-button').on('click', function (event) {
        event.preventDefault();
        $('#hctm-form tbody').append(newCtaTemplate);
        listenersAndDropdowns();
    });

    /**
     * Set up dropdowns and event listeners on the page
     * @return void
     */
    function listenersAndDropdowns () {
        /*
         * Dropdown functionality
         */
        $('#hctm-form select').chosen({width: "250px"})
            // assigning a unique name to the tags dropdowns
            // prevents issues that arise if no tags are chosen
            .each(function (i, elm) {
                $(elm).attr('name', 'tags[' + i + '][]');
            });

        /*
         * Row action button functionality
         */
        $('.hctm-delete-row').off('click') // remove existing listeners to prevent dupes
            .on('click', function (event) {
                event.preventDefault();
                if (confirm('Are you sure?')) {
                    $(this).closest('tr').remove();
                    listenersAndDropdowns();
                }
            });
        $('.hctm-up-row').off('click') // remove existing listeners to prevent dupes
            .on('click', function (event) {
                event.preventDefault();
                var thisRow = $(this).closest('tr'),
                    prevRow = thisRow.prev('tr');
                prevRow.before(thisRow);
                listenersAndDropdowns();
            });
        $('.hctm-down-row').off('click') // remove existing listeners to prevent dupes
            .on('click', function (event) {
                event.preventDefault();
                var thisRow = $(this).closest('tr'),
                    nextRow = thisRow.next('tr');
                nextRow.after(thisRow);
                listenersAndDropdowns();
            });
    }
    listenersAndDropdowns();
})(jQuery);
